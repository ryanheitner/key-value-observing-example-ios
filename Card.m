//
//  Card.m
//  CardCaptorChars
//
//  Created by Ryan Heitner on 26/12/2013.
//  Copyright (c) 2013 Ryan Heitner. All rights reserved.
//

#import "Card.h"

@implementation Card

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if([keyPath isEqualToString:@"cardCount"])
    {
        NSNumber *oldC = [change objectForKey:NSKeyValueChangeOldKey];
        NSNumber *newC = [change objectForKey:NSKeyValueChangeNewKey];
        if(oldC < newC)
        {
            [self gainedCard];
        }else
        {
            [self lostCard];
        }
    }
    
}

- (void)lostCard {
    NSLog(@"%@ has lost a card! Cards now: %@", self.cardName, self.cardCount);
    

}
- (void)gainedCard {
    NSLog(@"%@ has gained a card! Cards now: %@", self.cardName, self.cardCount);
    
}
@end
