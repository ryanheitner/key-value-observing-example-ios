//
//  ViewController.h
//  CardCaptorChars
//
//  Created by Ryan Heitner on 26/12/2013.
//  Copyright (c) 2013 Ryan Heitner. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
- (IBAction)addCard:(id)sender;
- (IBAction)minusCard:(id)sender;

@end
