//
//  AppDelegate.m
//  CardCaptorChars
//
//  Created by Ryan Heitner on 26/12/2013.
//  Copyright (c) 2013 Ryan Heitner. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.

    
    //Create and give the properties some values with KVC...
    self.c1 = [[Card alloc] init];
    [self.c1 setValue:@"King Hearts" forKey:@"cardName"];
    [self.c1 setValue:[NSNumber numberWithInt:20] forKey:@"cardCount"];

   // [self.c1 setValue:[NSNumber numberWithInt:20] forKey:@"cardCount"];
    
    self.c2 = [[Card alloc] init];
    [self.c2 setValue:@"Two Clubs" forKey:@"cardName"];
    [self.c2 setValue:[NSNumber numberWithInt:10] forKey:@"cardCount"];

    //[self.c2 setValue:[NSNumber numberWithInt:10] forKey:@"cardCount"];

    
    //Done! Now we are going to fetch the values using KVC.
    
    NSString *mainCharacter = [self.c1 valueForKey:@"cardName"];
    NSInteger mainCount = [[self.c1 valueForKey:@"cardCount"] integerValue];
    [self.c1 addObserver:self.c1 forKeyPath:@"cardCount" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
    
    NSString *rivalChar = [self.c2 valueForKey:@"cardName"];
    NSInteger rivalCount = [[self.c2 valueForKey:@"cardCount"] integerValue];
    [self.c2 addObserver:self.c2 forKeyPath:@"cardCount" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];

    
    NSLog(@"%@ has %d Clow Cards", mainCharacter, mainCount);
    NSLog(@"%@ has %d Clow Cards", rivalChar, rivalCount);

    
    
    return YES;
}
- (void)addCard{
    
    self.c1.cardCount = @([self.c1.cardCount intValue] + 1);
    
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
