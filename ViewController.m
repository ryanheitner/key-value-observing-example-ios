//
//  ViewController.m
//  CardCaptorChars
//
//  Created by Ryan Heitner on 26/12/2013.
//  Copyright (c) 2013 Ryan Heitner. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addCard:(id)sender {
    [[[UIApplication sharedApplication] delegate] performSelector:@selector(addCard)];
}

- (IBAction)minusCard:(id)sender {
}
@end
