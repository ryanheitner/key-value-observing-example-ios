//
//  Card.h
//  CardCaptorChars
//
//  Created by Ryan Heitner on 26/12/2013.
//  Copyright (c) 2013 Ryan Heitner. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Card : UITableViewCell
@property (nonatomic,copy)NSString *cardName;
@property (nonatomic,copy)NSNumber *cardCount;

- (void) lostCard;
    

- (void) gainedCard;


@end
