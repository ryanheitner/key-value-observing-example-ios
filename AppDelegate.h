//
//  AppDelegate.h
//  CardCaptorChars
//
//  Created by Ryan Heitner on 26/12/2013.
//  Copyright (c) 2013 Ryan Heitner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Card.h"



@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic) Card *c1;
@property (strong,nonatomic) Card *c2;

-(void)addCard;

@end
